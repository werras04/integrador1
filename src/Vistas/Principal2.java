package Vistas;

import Clases.*;
import Controlador.Negocio;
import java.awt.Component;
import java.awt.Toolkit; //sonido
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class Principal2 extends javax.swing.JFrame {

  int LayoutX;
  int LayoutY;
  Samsung Sams = new Samsung();
  Huawei Huaw = new Huawei();
  Apple Apple = new Apple();
  Negocio obj = new Negocio();
  double Sam = 0;
  double Hua = 0;
  double App = 0;
  DecimalFormat formato1 = new DecimalFormat("0.00");

  public Principal2() {
    initComponents();
    setLocationRelativeTo(null);
    for (Component a : PanelCalidad.getComponents()) {
      a.setEnabled(false);
    }
    Fecha.setText(Fecha());
  }

  public static String Fecha() {
    Date F = new Date();//Creacion de Fecha
    SimpleDateFormat ForFech = new SimpleDateFormat("dd/MM/YYYY");//Formato Fecha
    return ForFech.format(F);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel5 = new javax.swing.JPanel();
    jPanel1 = new javax.swing.JPanel();
    TITULO = new javax.swing.JLabel();
    CERRA = new javax.swing.JLabel();
    MINIMIZAR = new javax.swing.JLabel();
    MAXIMIZAR = new javax.swing.JLabel();
    PanelPadre = new javax.swing.JTabbedPane();
    panelRegistro = new javax.swing.JPanel();
    jPanel4 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    Fecha = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    txtNombre = new javax.swing.JTextField();
    txtDNI = new javax.swing.JTextField();
    txtTelefono = new javax.swing.JTextField();
    ComboMarca = new javax.swing.JComboBox<>();
    PanelCalidad = new javax.swing.JPanel();
    jLabel6 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel9 = new javax.swing.JLabel();
    camara = new javax.swing.JComboBox<>();
    bateria = new javax.swing.JComboBox<>();
    pantalla = new javax.swing.JComboBox<>();
    Rendimiento = new javax.swing.JComboBox<>();
    btnAgregar = new javax.swing.JButton();
    btnLimpiar = new javax.swing.JButton();
    jLabel10 = new javax.swing.JLabel();
    jLabel11 = new javax.swing.JLabel();
    panelListado = new javax.swing.JPanel();
    jPanel2 = new javax.swing.JPanel();
    jPanel3 = new javax.swing.JPanel();

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 100, Short.MAX_VALUE)
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 100, Short.MAX_VALUE)
    );

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setUndecorated(true);
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jPanel1.setBackground(new java.awt.Color(76, 41, 211));
    jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
      public void mouseDragged(java.awt.event.MouseEvent evt) {
        jPanel1MouseDragged(evt);
      }
    });
    jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mousePressed(java.awt.event.MouseEvent evt) {
        jPanel1MousePressed(evt);
      }
    });
    jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    TITULO.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    TITULO.setForeground(new java.awt.Color(255, 255, 255));
    TITULO.setText("Calculadora estadística que emplea medidas de dispersión para evaluar la calidad de los equipos móviles");
    jPanel1.add(TITULO, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 680, 30));

    CERRA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_close_window_25px_1.png"))); // NOI18N
    CERRA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    CERRA.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
        CERRAMouseClicked(evt);
      }
    });
    jPanel1.add(CERRA, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 40, 30));

    MINIMIZAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_minimize_window_30px.png"))); // NOI18N
    MINIMIZAR.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    MINIMIZAR.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
        MINIMIZARMouseClicked(evt);
      }
    });
    jPanel1.add(MINIMIZAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 0, 40, 30));

    MAXIMIZAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_maximize_button_20px.png"))); // NOI18N
    MAXIMIZAR.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    MAXIMIZAR.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
        MAXIMIZARMouseClicked(evt);
      }
    });
    jPanel1.add(MAXIMIZAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, 40, 30));

    getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 30));

    PanelPadre.setBorder(new javax.swing.border.MatteBorder(null));
    PanelPadre.setTabPlacement(javax.swing.JTabbedPane.LEFT);
    PanelPadre.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

    panelRegistro.setBackground(new java.awt.Color(255, 255, 255));
    panelRegistro.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jPanel4.setBackground(new java.awt.Color(0, 141, 203));

    jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
    jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel1.setText("REGISTRARSE");

    Fecha.setBackground(new java.awt.Color(255, 255, 255));
    Fecha.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel4Layout.createSequentialGroup()
        .addGap(44, 44, 44)
        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(Fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(115, Short.MAX_VALUE))
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
      .addComponent(Fecha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    panelRegistro.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 19, -1, -1));

    jLabel2.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
    jLabel2.setForeground(new java.awt.Color(0, 141, 203));
    jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel2.setText("NOMBRE");
    panelRegistro.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(98, 95, 142, 30));

    jLabel3.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(0, 141, 203));
    jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel3.setText("TELEFONO");
    panelRegistro.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 142, 43));

    jLabel4.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
    jLabel4.setForeground(new java.awt.Color(0, 141, 203));
    jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel4.setText("ESCOJA MARCA DE CELULAR");
    panelRegistro.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(67, 266, 207, 43));

    jLabel5.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
    jLabel5.setForeground(new java.awt.Color(0, 141, 203));
    jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel5.setText("DNI");
    panelRegistro.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(98, 212, 142, 43));

    txtNombre.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    txtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 141, 203), 2));
    txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyTyped(java.awt.event.KeyEvent evt) {
        txtNombreKeyTyped(evt);
      }
    });
    panelRegistro.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 90, 287, 30));

    txtDNI.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    txtDNI.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 141, 203), 2));
    panelRegistro.add(txtDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 210, 287, 32));

    txtTelefono.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    txtTelefono.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 141, 203), 2));
    panelRegistro.add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 287, 32));

    ComboMarca.setBackground(new java.awt.Color(76, 41, 211));
    ComboMarca.setForeground(new java.awt.Color(255, 255, 255));
    ComboMarca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ELEGIR", "SAMSUNG", "HUAWEI", "APPLE" }));
    ComboMarca.setFocusable(false);
    ComboMarca.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        ComboMarcaActionPerformed(evt);
      }
    });
    panelRegistro.add(ComboMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 280, 110, -1));

    PanelCalidad.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CALIDAD", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 1, 14))); // NOI18N

    jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel6.setText("CALIDAD DE CAMARA");

    jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel7.setText("CALIDAD DE BATERIA");

    jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel8.setText("CALIDAD DE PANTALLA");

    jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
    jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    jLabel9.setText("CALIDAD DE RENDIMIENTO");

    camara.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ELEGIR", "PESIMO(1)", "REGULAR(2)", "BUENO(3)", "EXCELENTE(4)" }));
    camara.setFocusable(false);

    bateria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ELEGIR", "PESIMO", "REGULAR", "BUENO", "EXCELENTE" }));
    bateria.setFocusable(false);

    pantalla.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ELEGIR", "PESIMO", "REGULAR", "BUENO", "EXCELENTE" }));
    pantalla.setFocusable(false);

    Rendimiento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ELEGIR", "PESIMO", "REGULAR", "BUENO", "EXCELENTE" }));
    Rendimiento.setFocusable(false);

    javax.swing.GroupLayout PanelCalidadLayout = new javax.swing.GroupLayout(PanelCalidad);
    PanelCalidad.setLayout(PanelCalidadLayout);
    PanelCalidadLayout.setHorizontalGroup(
      PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(PanelCalidadLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(camara, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(bateria, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(pantalla, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(Rendimiento, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addGap(23, 23, 23))
    );
    PanelCalidadLayout.setVerticalGroup(
      PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(PanelCalidadLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(camara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(bateria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(pantalla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(18, 18, 18)
        .addGroup(PanelCalidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(Rendimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    panelRegistro.add(PanelCalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, 370, 190));

    btnAgregar.setText("AGREGAR");
    panelRegistro.add(btnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 370, 110, 30));

    btnLimpiar.setText("LIMPIAR");
    btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnLimpiarActionPerformed(evt);
      }
    });
    panelRegistro.add(btnLimpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 440, 110, 30));

    jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_checked_checkbox_30px.png"))); // NOI18N
    panelRegistro.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 370, -1, -1));

    jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_broom_30px.png"))); // NOI18N
    panelRegistro.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 440, -1, -1));

    PanelPadre.addTab("REGISTRAR DATOS", panelRegistro);

    javax.swing.GroupLayout panelListadoLayout = new javax.swing.GroupLayout(panelListado);
    panelListado.setLayout(panelListadoLayout);
    panelListadoLayout.setHorizontalGroup(
      panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 681, Short.MAX_VALUE)
    );
    panelListadoLayout.setVerticalGroup(
      panelListadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 563, Short.MAX_VALUE)
    );

    PanelPadre.addTab("LISTADO DE DATOS", panelListado);

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 681, Short.MAX_VALUE)
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 563, Short.MAX_VALUE)
    );

    PanelPadre.addTab("tab3", jPanel2);

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 681, Short.MAX_VALUE)
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 563, Short.MAX_VALUE)
    );

    PanelPadre.addTab("tab4", jPanel3);

    getContentPane().add(PanelPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 800, 570));

    pack();
  }// </editor-fold>//GEN-END:initComponents
//PARA CERRAR EL SISTEMA
  private void CERRAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CERRAMouseClicked
    if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
      System.exit(0);
    }
  }//GEN-LAST:event_CERRAMouseClicked
//PARA MAXIMIZAR EL SISTEMA
  private void MAXIMIZARMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MAXIMIZARMouseClicked
    if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
      this.setExtendedState(MAXIMIZED_BOTH);
    }
  }//GEN-LAST:event_MAXIMIZARMouseClicked
//PARA MINIMIZAR EL SISTEMA
  private void MINIMIZARMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MINIMIZARMouseClicked
    if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
      this.setExtendedState(ICONIFIED);
    }
  }//GEN-LAST:event_MINIMIZARMouseClicked
// DARLE MOVIMIENTO AL SISTEMA
  private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
    if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
      LayoutX = evt.getX();
      LayoutY = evt.getY();
    }
  }//GEN-LAST:event_jPanel1MousePressed

  private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
    this.setLocation(evt.getXOnScreen() - LayoutX, evt.getYOnScreen() - LayoutY);

  }//GEN-LAST:event_jPanel1MouseDragged
// BOTON PARA LIMPIAR
  private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    btnLimpiar.setEnabled(true);
    txtNombre.setText("");
    txtTelefono.setText("");
    txtDNI.setText("");
    camara.setSelectedIndex(0);
    bateria.setSelectedIndex(0);
    pantalla.setSelectedIndex(0);
    Rendimiento.setSelectedIndex(0);
    ComboMarca.setSelectedIndex(0);
    txtNombre.requestFocus();
    JOptionPane.showMessageDialog(null, "Limpieza Realizada");
  }//GEN-LAST:event_btnLimpiarActionPerformed
//COMBO DE LA MARCA
  private void ComboMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboMarcaActionPerformed
    int tipo = 0;
    tipo = ComboMarca.getSelectedIndex();
    if (tipo == 0) {
      for (Component a : PanelCalidad.getComponents()) {
        a.setEnabled(false);
      }
    } else {
      for (Component b : PanelCalidad.getComponents()) {
        b.setEnabled(true);
      }
    }
  }//GEN-LAST:event_ComboMarcaActionPerformed

  private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
    char nombre = evt.getKeyChar();
        if (Character.isSpaceChar(nombre)) {
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
        } else if (!Character.isLetter(nombre)) {
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
        }
  }//GEN-LAST:event_txtNombreKeyTyped

  public static void main(String args[]) {

    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new Principal2().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel CERRA;
  private javax.swing.JComboBox<String> ComboMarca;
  private javax.swing.JLabel Fecha;
  private javax.swing.JLabel MAXIMIZAR;
  private javax.swing.JLabel MINIMIZAR;
  private javax.swing.JPanel PanelCalidad;
  private javax.swing.JTabbedPane PanelPadre;
  private javax.swing.JComboBox<String> Rendimiento;
  private javax.swing.JLabel TITULO;
  private javax.swing.JComboBox<String> bateria;
  private javax.swing.JButton btnAgregar;
  private javax.swing.JButton btnLimpiar;
  private javax.swing.JComboBox<String> camara;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel4;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JPanel panelListado;
  private javax.swing.JPanel panelRegistro;
  private javax.swing.JComboBox<String> pantalla;
  private javax.swing.JTextField txtDNI;
  private javax.swing.JTextField txtNombre;
  private javax.swing.JTextField txtTelefono;
  // End of variables declaration//GEN-END:variables
}
