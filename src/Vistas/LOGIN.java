
package Vistas;

import javax.swing.JOptionPane;

public class LOGIN extends javax.swing.JFrame {


  public LOGIN() {
    initComponents();
    setLocationRelativeTo(null);
  }

  
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    txtTitulo = new javax.swing.JLabel();
    txtLogo = new javax.swing.JLabel();
    txtLogo2 = new javax.swing.JLabel();
    jPanel2 = new javax.swing.JPanel();
    txtitu = new javax.swing.JButton();
    txtUsuario = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    txtColocarUsuario = new javax.swing.JTextField();
    txtColocarContraseña = new javax.swing.JPasswordField();
    jSeparator1 = new javax.swing.JSeparator();
    jSeparator2 = new javax.swing.JSeparator();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    BtnIngresar = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jPanel1.setBackground(new java.awt.Color(73, 181, 172));

    txtTitulo.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
    txtTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    txtTitulo.setText("<html><center>Calculadora estadística que emplea medidas de dispersión para evaluar la calidad de los equipos móviles<html>");

    txtLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    txtLogo.setIcon(new javax.swing.ImageIcon("C:\\Users\\pc\\Desktop\\Captura.PNG")); // NOI18N

    txtLogo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_calculator_25px_1.png"))); // NOI18N

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(63, 63, 63)
            .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(txtLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(txtLogo2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addContainerGap(47, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addComponent(txtLogo2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(38, 38, 38)
        .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(29, 29, 29)
        .addComponent(txtLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(35, Short.MAX_VALUE))
    );

    getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 410, 600));

    jPanel2.setBackground(new java.awt.Color(33, 45, 62));
    jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    txtitu.setFont(new java.awt.Font("Gulim", 0, 36)); // NOI18N
    txtitu.setForeground(new java.awt.Color(255, 255, 255));
    txtitu.setText("INICIAR SESIÓN");
    txtitu.setContentAreaFilled(false);
    txtitu.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        txtituActionPerformed(evt);
      }
    });
    jPanel2.add(txtitu, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, -1, 44));

    txtUsuario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    txtUsuario.setForeground(new java.awt.Color(255, 255, 255));
    txtUsuario.setText("USUARIO");
    jPanel2.add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(103, 212, -1, -1));

    jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(255, 255, 255));
    jLabel3.setText("CONTRASEÑA");
    jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(103, 322, -1, -1));

    txtColocarUsuario.setBackground(new java.awt.Color(33, 45, 62));
    txtColocarUsuario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    txtColocarUsuario.setForeground(new java.awt.Color(255, 255, 255));
    txtColocarUsuario.setBorder(null);
    txtColocarUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusGained(java.awt.event.FocusEvent evt) {
        txtColocarUsuarioFocusGained(evt);
      }
    });
    txtColocarUsuario.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        txtColocarUsuarioActionPerformed(evt);
      }
    });
    jPanel2.add(txtColocarUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 240, 220, 30));

    txtColocarContraseña.setBackground(new java.awt.Color(33, 45, 62));
    txtColocarContraseña.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    txtColocarContraseña.setForeground(new java.awt.Color(255, 255, 255));
    txtColocarContraseña.setBorder(null);
    jPanel2.add(txtColocarContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 350, 220, 30));

    jSeparator1.setBackground(new java.awt.Color(73, 181, 172));
    jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(122, 292, 241, 12));

    jSeparator2.setBackground(new java.awt.Color(73, 181, 172));
    jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 390, 239, 10));

    jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_lock_25px.png"))); // NOI18N
    jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 350, 54, 30));

    jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_user_25px.png"))); // NOI18N
    jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 240, 30, 30));

    BtnIngresar.setBackground(new java.awt.Color(73, 181, 172));
    BtnIngresar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    BtnIngresar.setText("INGRESAR");
    BtnIngresar.setBorder(null);
    BtnIngresar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    BtnIngresar.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
        BtnIngresarMouseClicked(evt);
      }
    });
    jPanel2.add(BtnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 460, 200, 60));

    getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(367, 0, 490, 600));

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void txtColocarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtColocarUsuarioActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_txtColocarUsuarioActionPerformed

  private void txtituActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtituActionPerformed
   
  }//GEN-LAST:event_txtituActionPerformed

  private void BtnIngresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnIngresarMouseClicked
    String Usuario="admin";
    String Contraseña="123";
    
    String Pass=new String(txtColocarContraseña.getPassword());
    if(txtColocarUsuario.getText().equalsIgnoreCase(Usuario) && Pass.equals(Contraseña)){
      Principal Pr=new Principal();
      Pr.setVisible(true);
      dispose();
    }else{
      JOptionPane.showMessageDialog(this,"Usuario / Contraseña es incorrecta");
    }
  }//GEN-LAST:event_BtnIngresarMouseClicked

  private void txtColocarUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtColocarUsuarioFocusGained
    // TODO add your handling code here:
  }//GEN-LAST:event_txtColocarUsuarioFocusGained

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Windows".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new LOGIN().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton BtnIngresar;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JPasswordField txtColocarContraseña;
  private javax.swing.JTextField txtColocarUsuario;
  private javax.swing.JLabel txtLogo;
  private javax.swing.JLabel txtLogo2;
  private javax.swing.JLabel txtTitulo;
  private javax.swing.JLabel txtUsuario;
  private javax.swing.JButton txtitu;
  // End of variables declaration//GEN-END:variables
}
