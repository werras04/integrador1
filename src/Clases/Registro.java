package Clases;
public class Registro {

    private int Codigo;
    private String Nombre;
    private int NumTel;//Numero de Telefono
    private int DNI;
    private int Tipo;
    private int Calidad;
    static int Cuenta = 1001;

    public Registro() {
        this.Codigo = Cuenta++;
    }

    public Registro(String Nombre, int NumTel, int DNI, int Tipo, int Calidad) {
        this.Codigo = Cuenta++;
        this.Nombre = Nombre;
        this.NumTel = NumTel;
        this.DNI = DNI;
        this.Tipo = Tipo;
        this.Calidad = Calidad;
    }

    public String Marca() {
        String vec[] = {"", "Samsung", "Huawei", "Apple"};
        return vec[Tipo];
    }

    public int Cal() {
        int vec[] = {0, 1, 2, 3, 4, 5};
        return vec[Calidad];
    }

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getNumTel() {
        return NumTel;
    }

    public void setNumTel(int NumTel) {
        this.NumTel = NumTel;
    }

    public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int Tipo) {
        this.Tipo = Tipo;
    }

    public int getCalidad() {
        return Calidad;
    }

    public void setCalidad(int Calidad) {
        this.Calidad = Calidad;
    }

}
