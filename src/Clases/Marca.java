package Clases;
public abstract class Marca {

    private int Intevalo;//Intervalos 
    private int Intevalo1;
    private int Intevalo2;
    private int Intevalo3;

    public Marca() {
    }

    public Marca(int Intevalo, int Intevalo1, int Intevalo2, int Intevalo3) {
        this.Intevalo = Intevalo;
        this.Intevalo1 = Intevalo1;
        this.Intevalo2 = Intevalo2;
        this.Intevalo3 = Intevalo3;
    }

    public int VecInter(int v) {//Vector de los Intervalos
        int Vec[] = {Intevalo, Intevalo1, Intevalo2, Intevalo3};
        return Vec[v];
    }

    public void cont(int cali) {//Contar
        switch (cali) {
            case 1:
                Intevalo++;
                break;
            case 2:

                Intevalo1++;
                break;
            case 3:

                Intevalo2++;
                break;
            case 4:

                Intevalo3++;
                break;

        }

    }

    public double PromInter(int p) {//Promedio de los Intervalos
        double V[] = {1.5, 2.5, 3.5, 4.5};
        return V[p];
    }

    public double Multi(int m) {//Multiplicacion fi*xi
        double Vec[] = new double[4];
        for (int i = 0; i < 4; i++) {
            Vec[i] = VecInter(i) * PromInter(i);
        }
        return Vec[m];
    }

    public double Multi2(int m) {//Multiplicacion fi*xi^2
        double Vec[] = new double[4];
        for (int i = 0; i < 4; i++) {
            Vec[i] = VecInter(i) * Math.pow(PromInter(i), 2);
        }
        return Vec[m];
    }

    public double[][] Matriz() {
        double[][] vec = new double[4][4];
        for (int i = 0; i < 4; i++) {

            for (int j = 0; j < 4; j++) {
                if (j == 0) {
                    vec[i][j] = VecInter(i);
                } else if (j == 1) {
                    vec[i][j] = PromInter(i);
                }
                if (j == 2) {
                    vec[i][j] = Multi(i);
                }
                if (j == 3) {
                    vec[i][j] = Multi2(i);
                }
            }

        }
        return vec;
    }

    public int SumInter() {//Suma total para hallar la muestra
        int s = 0;
        for (int i = 0; i < 4; i++) {
            s = s + VecInter(i);
        }
        return s;
    }

    public double SumMul() {
        double s = 0;
        for (int i = 0; i < 4; i++) {
            s = s + Multi(i);
        }
        return s;
    }

    public double SumMul2() {
        double s = 0;
        for (int i = 0; i < 4; i++) {
            s = s + Multi2(i);
        }
        return s;
    }

    public double Media() {
        return SumMul() / SumInter();
    }

    public double Varianza() {
        return (SumMul2() - (SumInter() * (Media() * Media()))) / (SumInter() - 1);
    }

    public double Desviacion() {
        return Math.sqrt(Varianza());
    }

    public double CoeVaria() {
        return (Desviacion() / Media()) * 100;
    }

    public String InterCV() {
        String Inter;
        if (CoeVaria() < 10) {
            Inter = "se encuentra en el Primer Rango teniendo un comportamiento Homogeneo";
        } else if (CoeVaria() < 30) {
            Inter = "se encuentra en el Segundo Rango teniendo un comportamiento de Variabilidad Aceptable";
        } else {
            Inter = "se Encuentra en el Tercer Rango teniendo un comportamiento Heterogeneo";
        }
   return Inter;
    }

}
