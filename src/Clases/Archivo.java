package Clases;
import java.io.File;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;

public class Archivo {
static FileOutputStream salida;

    public static String guardarTexto(File archivo, String contenido) {
        String respuesta = null;
        try {
            salida = new FileOutputStream(archivo);
            byte[] bytesTxt = contenido.getBytes();
            salida.write(bytesTxt);
            respuesta = "Los datos se guardaron exitosamente";
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Hubo un error al momento de guardar el texto");
        }

        return respuesta;
    }
}